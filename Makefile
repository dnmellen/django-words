testenv:
	pip install -r requirements-dev.txt

test:
	flake8 django_words --ignore=E501,E128,E701,E261,E301,E126,E127
	coverage run django_words/manage.py test
	coverage report

.PHONY: test
