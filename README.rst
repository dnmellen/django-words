==============
Django words
==============


Sample Django project using models, forms and REST APIs


Features
--------

- Django administration
- User authentication
- Web app to update word skills per user
- REST API to get all words for one user


Quickstart
----------

.. code-block :: bash

    $ git clone https://bitbucket.org/dnmellen/django-words.git
    $ cd django-words
    $ mkvirtualenv django-words
    $ pip install -r requirements-dev.txt
    $ make test
    $ python django_words/manage.py runserver

- Go to django admin (http://127.0.0.1:8000/admin/) and add some users, words and skills.
- Go to http://127.0.0.1:8000 to update the skills for the current user.
- Use http://127.0.0.1:8000/api/word-skills/ to get words info for the current user in JSON format.
