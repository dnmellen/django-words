import json
from django.core.urlresolvers import reverse
from django.test import TestCase
from .utils import UserFixtureMixin
from .models import Word, Kind, Mastery


class TestRestApi(UserFixtureMixin, TestCase):

    def setUp(self):
        super(TestRestApi, self).setUp()

        self.kind_noun = Kind.objects.create(name='noun')
        self.kind_adj = Kind.objects.create(name='adjetive')
        self.word_chair = Word.objects.create(name='chair', kind=self.kind_noun)
        self.word_red = Word.objects.create(name='red', kind=self.kind_adj)
        Mastery.objects.create(user=self.user, word=self.word_chair, skill=0.5)
        Mastery.objects.create(user=self.user, word=self.word_red, skill=0.75)

    def test_get_all_words(self):
        raw_response = self.client.get(reverse('words:api_word_skills'))
        response = json.loads(raw_response.content)
        expected_result = {
            'data': [{'skill': 0.5, 'word_id': 3, 'word_kind': 'noun', 'word_name': 'chair'},
                     {'skill': 0.75, 'word_id': 4, 'word_kind': 'adjetive', 'word_name': 'red'}]
        }
        self.assertEqual(response, expected_result)
