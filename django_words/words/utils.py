"""
Nice shortcuts for making tests
"""

from django.test.client import Client
from django.contrib.auth import get_user_model


class UserFixtureMixin(object):

    user_name = 'john'
    user_pass = 'john'
    user_email = 'john@john.com'

    def setUp(self):
        self.user = get_user_model().objects.create_user(self.user_name,
                                                         self.user_email,
                                                         self.user_pass)
        self.client = Client()
        self.client.login(username=self.user_name, password=self.user_pass)
