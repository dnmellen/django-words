from django.contrib import admin
from .models import Word, Kind, Mastery


class WordAdmin(admin.ModelAdmin):
    fields = ('name', 'kind', 'created_on', 'modified_on')
    readonly_fields = ('created_on', 'modified_on')
    list_display = ('name', 'kind')
    list_filter = ['kind__name']
    search_fields = ['name']

admin.site.register(Word, WordAdmin)


class KindAdmin(admin.ModelAdmin):
    fields = ('name', 'created_on', 'modified_on')
    readonly_fields = ('created_on', 'modified_on')
    list_display = ('name',)
    search_fields = ['name']

admin.site.register(Kind, KindAdmin)


class MasteryAdmin(admin.ModelAdmin):
    fields = ('user', 'word', 'skill')
    list_display = ('user', 'word', 'skill')
    list_filter = ['word__kind__name']
    search_fields = ['user__name', 'word__name']

admin.site.register(Mastery, MasteryAdmin)
