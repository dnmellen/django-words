from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView
from django.contrib import messages
from braces.views import LoginRequiredMixin, JSONResponseMixin
from extra_views.formsets import ModelFormSetView
from .models import Mastery
from .forms import MasteryForm


class UserBiasedMixin(object):
    """
    Overrides get_queryset to filter out results by current user
    """

    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super(UserBiasedMixin, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super(UserBiasedMixin, self).get_queryset().filter(user=self.user)


class MasteryWordsView(LoginRequiredMixin, UserBiasedMixin, ModelFormSetView):
    model = Mastery
    form_class = MasteryForm
    template_name = 'mastery_formset.html'
    extra = 0
    success_message = 'Skills updated ok'
    success_url = reverse_lazy('words:skills')

    def formset_valid(self, formset):
        messages.success(self.request, self.success_message)
        return super(MasteryWordsView, self).formset_valid(formset)


class UserMasteryView(LoginRequiredMixin, UserBiasedMixin, JSONResponseMixin, ListView):
    model = Mastery

    def get(self, request, *args, **kwargs):
        super(UserMasteryView, self).get(request, *args, **kwargs)
        context = self.get_context_data()
        response = {
            'data': [{'word_name': mastery.word.name,
                      'word_kind': mastery.word.kind.name,
                      'word_id': mastery.word.id,
                      'skill': mastery.skill} for mastery in context['object_list']]
        }
        return self.render_json_response(response)
