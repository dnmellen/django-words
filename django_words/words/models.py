from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


class TimeAwareModel(models.Model):
    created_on = models.DateTimeField(_('Created on'), auto_now_add=True)
    modified_on = models.DateTimeField(_('Modified on'), auto_now=True)


@python_2_unicode_compatible
class Word(TimeAwareModel):
    """
    A word is defined by a name and its kind.
    A word also is related to the user, since the user has the ability
    to learn new words and we can measure the mastery level the user has
    for each word.
    """

    name = models.CharField(_('Name'), max_length=50)
    kind = models.ForeignKey('Kind', verbose_name=_('Kind'))
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, through='Mastery')

    class Meta:
        verbose_name = _('Word')
        verbose_name_plural = _('Words')
        unique_together = ('name', 'kind')

    def __str__(self):
        return "{} ({})".format(self.name, self.kind.name)


@python_2_unicode_compatible
class Kind(TimeAwareModel):
    """
    This model defines the kind of a word (noun, adjetive, etc...)
    """

    name = models.CharField(_('Name'), max_length=50, unique=True)

    class Meta:
        verbose_name = _('Kind')
        verbose_name_plural = _('Kinds')

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Mastery(models.Model):
    """
    Mastery is the level of knowledge that the user has on a specific word.
    This level or skill is measured from 0 to 1.
    """

    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    word = models.ForeignKey('Word')
    skill = models.FloatField(_('Skill'), default=0.0, validators=[MaxValueValidator(1.0), MinValueValidator(0.0)])

    class Meta:
        verbose_name = _('Mastery')
        verbose_name_plural = _('Masterys')
        unique_together = ('user', 'word')
        ordering = ['word__name']

    def __str__(self):
        return "{} - {} ({:.2f})".format(self.user.username, self.word.name, self.skill)
