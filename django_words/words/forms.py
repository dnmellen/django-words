from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Fieldset
import floppyforms as forms
from .models import Mastery


class MasteryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(MasteryForm, self).__init__(*args, **kwargs)
        # Crispy form
        self.fields['id'] = forms.IntegerField(required=True, widget=forms.HiddenInput)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Fieldset(
                '{} ({})'.format(self.instance.word.name, self.instance.word.kind.name),
                Field('id'),
                Field('skill', step='0.01'),
            ),
        )
        self.helper.form_tag = False

    class Meta:
        model = Mastery
        fields = ('skill',)
