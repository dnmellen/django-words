from django.conf.urls import patterns, url
from .views import MasteryWordsView, UserMasteryView

urlpatterns = patterns(
    '',
    url(r'^$', MasteryWordsView.as_view(), name='skills'),
    url(r'^api/word-skills/$', UserMasteryView.as_view(), name='api_word_skills')
)
