from django.test import TestCase
from .utils import UserFixtureMixin
from .models import Word, Kind, Mastery


class TestWordsModels(UserFixtureMixin, TestCase):

    def setUp(self):
        super(TestWordsModels, self).setUp()

        self.kind_noun = Kind.objects.create(name='noun')
        self.kind_adj = Kind.objects.create(name='adjetive')
        self.word_chair = Word.objects.create(name='chair', kind=self.kind_noun)
        self.word_red = Word.objects.create(name='chair', kind=self.kind_adj)

    def test_add_mastery_valid_skill(self):
        m1 = Mastery.objects.create(user=self.user, word=self.word_chair, skill=0.5)
        self.assertEquals(m1.skill, 0.5)
